swagger: "2.0"
info:
  description: "Specification of a tavern service to offer and seek help."
  version: "1.0.0"
  title: "QuestWithREST"

host: "localhost:8080"
basePath: "/"

tags:
- name: "offers"
  description: "Everything about offers"
  
schemes:
- "http"
paths:
  /taverna/offers:
    get:
      tags:
      - "offers"
      summary: "Get all offers of the tavern"
      description: "Get all offers of the tavern as a list.<br/>It is possible but not necessary to filter the result by one specific hero class.<br/><br/><b><i>Example calls:</i></b><br/>/taverna/offerse<br/>/taverna/offers?only-hero-class=mage"
      parameters:
      - in: "query"
        name: "only-hero-class"
        description: "The only class of the hero which should offer the help<br/>See also: #/definitions/HeroClass<br/><br/><b><i>Example: mage</i></b>"
        required: false
        type: "string"
      responses:
        200:
          description: "OK: Operation was successful!"
          schema:
            type: array
            items:
              $ref: "#/definitions/Offer"
        204:
          description: "No Content: Operation was successful, but no offers where found!"
        400:
          description: "Bad Request: Invalid hero class!"
    post:
      tags:
      - "offers"
      summary: "Add a new offer to the tavern"
      description: "Add a new offer to the tavern by passing new offer into the body."
      parameters:
      - in: "body"
        name: "body"
        description: "The offer that needs to be added to the tavern"
        required: true
        schema:
          $ref: "#/definitions/Offer"
      responses:
        201:
          description: "Created: Operation was successful!"
          schema:
            $ref: "#/definitions/Offer"
        400:
          description: "Bad Request: Invalid attribute values supplied!"
        409:
          description: "Conflict: Another offer with same ID already exists!"
  /taverna/offers/{offer-id}:
    put:
      tags:
      - "offers"
      summary: "Update an existing offer"
      description: "Update an existing offer (identified by its id) by passing new offer attribute values into the body."
      parameters:
      - in: "path"
        name: "offer-id"
        description: "The id of the offer that should be updated"
        required: true
        type: string
      - in: "body"
        name: "body"
        description: "The offer with new attribute values that needs to be updated"
        required: true
        schema:
          $ref: "#/definitions/Offer"
      responses:
        200:
          description: "OK: Operation was successful!"
          schema:
            $ref: "#/definitions/Offer"
        400:
          description: "Bad Request: Invalid attribute values supplied!"
        404:
          description: "Not Found: Offer not found!"
    delete:
      tags:
      - "offers"
      summary: "Delete an existing offer"
      description: "Delete an existing offer by id."
      parameters:
      - in: "path"
        name: "offer-id"
        description: "The id of the offer that should be deleted"
        required: true
        type: string
      responses:
        200:
          description: "OK: Operation was successful!"
          schema:
            $ref: "#/definitions/Offer"
        404:
          description: "Not Found: Offer not found!"

definitions:
  HeroClass:
    description: "The class of the hero which offers the offer"
    type: string
    enum:
    - mage
    - warrior
    - priest
    - ...
    example: mage
  Offer:
    type: "object"
    properties:
      id:
        description: "The unique identifier of the offer"
        type: "integer"
        format: "int64"
        example: 0
      user:
        description: "The name of the user with is associated to the hero which offers the offer"
        type: "string"
        example: "myuser"
      hero-name:
        description: "The name of the hero which offers the offer"
        type: "string"
        example: "myhero"
      hero-class:
        $ref: "#/definitions/HeroClass"
      hero-location:
        description: "The location of the hero which offers the offer"
        type: "string"
        example: "Thronehall"
