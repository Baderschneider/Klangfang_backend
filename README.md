# VSP Aufgabe 1

## Projekt klonen
- mit `git clone <link von oben>`

## Projekt kompilieren, starten, testen

### Für das Builden und Testen stehen folgende Gradle Tasks zur Verfügung:
(je nach System mit `./gradlew` oder `gradlew.bat` (Windows) im **Wurzelverzeichnis** ausführen)

- (Java 8 SDK installieren)
- Build & Test: `./gradlew build`
- Build & Test & integrierten Webserver (Tomcat) starten:`./gradlew bootRun` (Abbruch mit `Ctrl-C`); die Anwendung ist dann unter `http://localhost:8080` erreichbar.
