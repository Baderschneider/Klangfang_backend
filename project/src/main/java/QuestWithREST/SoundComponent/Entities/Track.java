package QuestWithREST.SoundComponent.Entities;

import com.fasterxml.jackson.annotation.JsonProperty;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.List;

@Entity
public class Track {


    @JsonProperty("id")
    @Id
    @NotNull
    @Column(name = "TRACK_ID")
    private int id;

    @NotNull
    @OneToMany(cascade = CascadeType.ALL)
    @JoinColumn(name="trackId")
    private List<Sound> sounds;


    public Track() {
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getId() {
        return id;
    }

    public void addSound(Sound sound){
        this.sounds.add(sound);
    }
}
