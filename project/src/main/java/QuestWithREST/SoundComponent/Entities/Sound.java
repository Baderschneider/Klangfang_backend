package QuestWithREST.SoundComponent.Entities;


import com.fasterxml.jackson.annotation.JsonProperty;
import javax.persistence.Id;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.OneToOne;
import javax.validation.constraints.NotNull;

@Entity
public class Sound {

    @NotNull
    @Id
    private int id;

    @JsonProperty("sid")
    @NotNull
    @OneToOne
    private SounfFile sounfFile;

    @JsonProperty("silenceInMs")
    @NotNull
    private int msOfSilence;


    public Sound() {
    }

    public Sound(SounfFile sounfFile, int msOfSilence) {
        this.sounfFile = sounfFile;
        this.msOfSilence = msOfSilence;
    }
}
