package QuestWithREST.SoundComponent.Entities;

import com.fasterxml.jackson.annotation.JsonProperty;

import javax.persistence.GeneratedValue;
import javax.persistence.Id;

import javax.persistence.Entity;
import javax.validation.constraints.NotNull;
import java.util.UUID;

@Entity
public class SounfFile {

    @JsonProperty("soundfileid")
    @Id
    @NotNull
    @GeneratedValue
    private int sounfFileId;

    @NotNull
    private UUID uuid;

    @NotNull
    private String path;

    public SounfFile(UUID uuid, String path) {
        this.uuid = uuid;
        this.path = path;
    }

    public SounfFile() {
    }

    public String toString(){
        return "{" +
                "\"sounfFileId\":" + "\"" +sounfFileId + "\"" + "" +
                ",\" UUID\":" + "\"" +uuid.toString() + "\""+ "" +
                ", \"path\":" + "\"" +path+ "\"" + "" +
                "}";

    }
}
