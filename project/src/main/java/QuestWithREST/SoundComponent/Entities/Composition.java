package QuestWithREST.SoundComponent.Entities;


import com.fasterxml.jackson.annotation.JsonProperty;
import javax.persistence.Id;
import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.List;

@Entity
public class Composition {

    @JsonProperty("id")
    @Id
    @NotNull
    @GeneratedValue
    @Column(name = "COMPOSITION_ID")
    private int id;

    @OneToMany(cascade = CascadeType.ALL)
    @JoinColumn(name="COMPOSITION_ID")
    private List<Track> tracks;


    public Composition() {
    }

    public int getId() {
        return id;
    }

    public int getTrackId(int TrackNo){
        return this.tracks.get(TrackNo).getId();
    }
}
