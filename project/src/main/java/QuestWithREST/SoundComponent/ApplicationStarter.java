package QuestWithREST.SoundComponent;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.boot.autoconfigure.orm.jpa.HibernateJpaAutoConfiguration;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.ApplicationListener;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;

@SpringBootApplication
@EnableAutoConfiguration()
public class ApplicationStarter {
    private static ConfigurableApplicationContext applicationContext;

    public static ConfigurableApplicationContext getApplicationContext() {
        return applicationContext;
    }

    @Bean
    CommandLineRunner init() {
        return args -> {
            Runtime.getRuntime().addShutdownHook(new Thread(() -> {
            }));
        };
    }

    @Component
    class ApplicationReadyEventListener implements ApplicationListener<ApplicationReadyEvent> {


        @Override
        public void onApplicationEvent(ApplicationReadyEvent event) {
        }
    }

    public static void main(String[] args) throws Exception {
        SpringApplication app = new SpringApplication(ApplicationStarter.class);
        ApplicationStarter.applicationContext = app.run(args);
    }
}
