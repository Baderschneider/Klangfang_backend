package QuestWithREST.SoundComponent.Components;

import QuestWithREST.SoundComponent.Entities.Sound;
import QuestWithREST.SoundComponent.Entities.SounfFile;
import QuestWithREST.SoundComponent.repositorys.SoundRepo;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.IOException;

@Component
public class SoundComponent {

    @Autowired
    SoundRepo soundRepo;

    @Autowired
    SoundFileComponent soundFileComponent;

    public String createSound(String data){
        ObjectMapper objectMapper = new ObjectMapper();

        JsonNode jsonNode = null;
        try {
            jsonNode = objectMapper.readTree(data);
        } catch (IOException e) {
            e.printStackTrace();
        }
        int sid = jsonNode.get("sid").asInt();
        int offset = jsonNode.get("offset").asInt();
        SounfFile sfile = soundFileComponent.getSoundFile(sid);
        Sound sound = new Sound(sfile,offset);
        return this.soundRepo.save(sound).toString();


    }

}
