package QuestWithREST.SoundComponent.Components;

import QuestWithREST.SoundComponent.Entities.SounfFile;
import QuestWithREST.SoundComponent.repositorys.SounfFileRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.UUID;

@Component
public class SoundFileComponent {

    @Autowired
    SounfFileRepo soundFileRepo;

    public SoundFileComponent() {
    }

    public String getAll() {
        return soundFileRepo.findAll().toString();
    }


    public void saveSoundFile(SounfFile sFile){
        soundFileRepo.save(sFile);
    }

    public String save(MultipartFile file) {
        System.out.println("saving file");
        byte[] bytes = new byte[0];
        try {
            bytes = file.getBytes();
        } catch (IOException e) {
            e.printStackTrace();
        }
        try {
            System.out.println("Saved it");
            Path path = Paths.get(UUID.nameUUIDFromBytes(file.getOriginalFilename().getBytes()).toString());
            UUID uuid = UUID.nameUUIDFromBytes(file.getOriginalFilename().getBytes());
            SounfFile sFile = new SounfFile(uuid,path.toString());
            Files.write(path, bytes);
            soundFileRepo.save(sFile);
            return sFile.toString();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return null;

    }

    public SounfFile getSoundFile(int sid) {
        return this.soundFileRepo.getOne(sid);
    }
}
