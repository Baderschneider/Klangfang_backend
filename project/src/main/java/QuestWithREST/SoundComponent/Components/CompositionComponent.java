package QuestWithREST.SoundComponent.Components;

import QuestWithREST.SoundComponent.Entities.Composition;
import QuestWithREST.SoundComponent.Entities.Sound;
import QuestWithREST.SoundComponent.Entities.Track;
import QuestWithREST.SoundComponent.repositorys.CompositionRepo;
import QuestWithREST.SoundComponent.repositorys.SoundRepo;
import QuestWithREST.SoundComponent.repositorys.TrackRepo;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

import java.io.IOException;

@Component
public class CompositionComponent {

    @Autowired
    CompositionRepo compositionRepo;
    @Autowired
    TrackRepo trackRepo;
    @Autowired
    SoundRepo soundRepo;

    public int createComposition(){

        Composition comp = new Composition();
        Composition compRepo = this.compositionRepo.save(comp);
        return compRepo.getId();
    }

    public String UpdateComposition(String data) {
        ObjectMapper objectMapper = new ObjectMapper();

        JsonNode jsonNode = null;
        try {
            jsonNode = objectMapper.readTree(data);
        } catch (IOException e) {
            e.printStackTrace();
        }
        int compositionId = jsonNode.get("compositionid").asInt();
        int trackNo = jsonNode.get("trackNo").asInt();
        int soundId = jsonNode.get("soundid").asInt();

        Composition composition = compositionRepo.getOne(compositionId);
        int trackid = composition.getTrackId(trackNo);
        Track track = trackRepo.getOne(trackid);
        Sound sound = soundRepo.getOne(soundId);

        track.addSound(sound);
        return trackRepo.save(track).toString();
    }
}
