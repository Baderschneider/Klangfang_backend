package QuestWithREST.SoundComponent;


import QuestWithREST.SoundComponent.Components.CompositionComponent;
import QuestWithREST.SoundComponent.Components.SoundComponent;
import QuestWithREST.SoundComponent.Components.SoundFileComponent;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

@RestController
public class KlangfangFacadeController {


    @Autowired
    SoundFileComponent soundFileComponent;

    @Autowired
    SoundComponent soundComponent;

    @Autowired
    CompositionComponent compositionComponent;

    @RequestMapping(value = "composition", method = RequestMethod.POST)
    public int getCompositions(){
        return compositionComponent.createComposition();
    }


    @RequestMapping(value = "composition", method = RequestMethod.GET)
    public ResponseEntity postCompositions() {
        return null;
    }

    @RequestMapping(value = "composition", method = RequestMethod.PUT)
    public String putCompositions(@RequestParam("data") String data) {
        return compositionComponent.UpdateComposition(data);
    }

    @RequestMapping(value = "soundfile", method = RequestMethod.POST)
    public String postSoundFile(@RequestParam("file") MultipartFile file) {
        return soundFileComponent.save(file);
    }

    @RequestMapping(value = "soundfile", method = RequestMethod.GET)
    public String getSoundFile() {
        return soundFileComponent.getAll();
    }

    @RequestMapping(value = "sound", method = RequestMethod.POST)
    public String PostSound(@RequestParam("data") String data) {
        return soundComponent.createSound(data);

    }

}
