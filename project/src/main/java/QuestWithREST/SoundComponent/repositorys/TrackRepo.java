package QuestWithREST.SoundComponent.repositorys;

import QuestWithREST.SoundComponent.Entities.SounfFile;
import QuestWithREST.SoundComponent.Entities.Track;
import org.springframework.data.jpa.repository.JpaRepository;

public interface TrackRepo extends JpaRepository<Track, Integer> {
}
