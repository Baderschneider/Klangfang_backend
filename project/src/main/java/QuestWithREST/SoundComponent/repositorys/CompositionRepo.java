package QuestWithREST.SoundComponent.repositorys;

import QuestWithREST.SoundComponent.Entities.Composition;
import QuestWithREST.SoundComponent.Entities.Sound;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CompositionRepo extends JpaRepository<Composition, Integer> {
}
