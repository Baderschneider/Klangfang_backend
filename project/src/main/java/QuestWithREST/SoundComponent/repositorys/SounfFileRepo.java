package QuestWithREST.SoundComponent.repositorys;


import QuestWithREST.SoundComponent.Entities.SounfFile;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface SounfFileRepo extends JpaRepository<SounfFile, Integer> {
}
